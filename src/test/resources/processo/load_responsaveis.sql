--
-- Sample dataset containing a number of Hotels in various Cities across the world.
--
delete from processo_responsaveis
delete from responsavel
delete from processo

insert into responsavel(id, nome, cpf, email, data_criacao) values (1, 'Isaias Souza', '50506549127', 'isaiassouza@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (2, 'Gabriela Souza', '36611615717', 'gabisouza@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (3, 'Mario Silva', '73368328735', 'mariosilva@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (4, 'Rafaela Campos', '71471421104', 'rafacamp@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (5, 'João Mario Filho', '87733530601', 'josemfilho@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (6, 'José Dias Leite', '77244653557', 'jdias@dominio.com', CURRENT_DATE)
