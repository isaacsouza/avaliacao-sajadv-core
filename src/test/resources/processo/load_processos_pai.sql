--
-- Sample dataset containing a number of Hotels in various Cities across the world.
--
insert into responsavel(id, nome, cpf, email, data_criacao) values (1, 'Isaias Souza', '50506549127', 'isaiassouza@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (2, 'Gabriela Souza', '36611615717', 'gabisouza@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (3, 'Mario Silva', '73368328735', 'mariosilva@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (4, 'Rafaela Campos', '71471421104', 'rafacamp@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (5, 'João Mario Filho', '87733530601', 'josemfilho@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (6, 'José Dias Leite', '77244653557', 'jdias@dominio.com', CURRENT_DATE)

insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (3000, '0000003032017JTROOO1', CURRENT_DATE, false, 'Pas003', 'Cliente está sob custódia', CURRENT_DATE, 3)

insert into processo_responsaveis (processo_id, responsavel_id) values (3000, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (3000, 4)
