--
-- Sample dataset containing a number of Hotels in various Cities across the world.
--
insert into responsavel(id, nome, cpf, email, data_criacao) values (1, 'Isaias Souza', '50506549127', 'isaiassouza@dominio.com', CURRENT_DATE)
insert into responsavel(id, nome, cpf, email, data_criacao) values (2, 'Gabriela Souza', '36611615717', 'gabisouza@dominio.com', CURRENT_DATE)

insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10101, '0000001012017JTROOO1', CURRENT_DATE, false, 'Pas001', null, CURRENT_DATE, 1)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10102, '0000002022017JTROOO1', CURRENT_DATE, false, 'Pas002', null, CURRENT_DATE, 1)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10103, '0000003032017JTROOO1', CURRENT_DATE, false, 'Pas003', null, CURRENT_DATE, 3)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10104, '0000004042017JTROOO1', CURRENT_DATE, true, 'Pas004', null, CURRENT_DATE, 3)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10105, '0000005052017JTROOO1', CURRENT_DATE, false, 'Pas005', null, CURRENT_DATE, 2)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10106, '0000006062017JTROOO1', CURRENT_DATE, false, 'Pas006', null, CURRENT_DATE, 2)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10107, '0000007072017JTROOO1', CURRENT_DATE, true, 'Pas007', null, CURRENT_DATE, 1)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10108, '0000008082017JTROOO1', CURRENT_DATE, false, 'Pas008', null, CURRENT_DATE, 1)
insert into processo(id, numero_processo_unificado, data_distribuicao, segredo_justica, pasta_fisica_cliente, descricao, data_criacao, situacao_id) values (10109, '0000009092017JTROOO1', CURRENT_DATE, false, 'Pas009', null, CURRENT_DATE, 1)

insert into processo_responsaveis (processo_id, responsavel_id) values (10101, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10102, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10102, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10103, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10103, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10104, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10104, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10105, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10106, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10107, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10107, 1)
insert into processo_responsaveis (processo_id, responsavel_id) values (10108, 2)
insert into processo_responsaveis (processo_id, responsavel_id) values (10109, 2)