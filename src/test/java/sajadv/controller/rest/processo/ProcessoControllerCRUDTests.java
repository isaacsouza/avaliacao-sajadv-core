package sajadv.controller.rest.processo;

import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcessoControllerCRUDTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcesso() throws Exception {

        String processo0000001012017JTROOO1 = IOUtils.toString(this.getClass().getResourceAsStream("/processo/0000001012017JTROOO1.json"));
        
        this.mvc.perform(
                post("/processos")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(processo0000001012017JTROOO1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.notNullValue()))
                .andExpect(jsonPath("$.descricao", Matchers.equalTo("Cliente está sob custódia")));
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos_pai.sql"})
    public void createProcessoComPai() throws Exception {

        String processo0000003022017JTROOO1 = IOUtils.toString(this.getClass().getResourceAsStream("/processo/0000003022017JTROOO1.json"));
        
        this.mvc.perform(
                post("/processos")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(processo0000003022017JTROOO1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.notNullValue()))
                .andExpect(jsonPath("$.descricao", Matchers.equalTo("Cliente normal")));
        
        this.mvc.perform(
                get("/processos").param("npu", "0000003022017JTROOO1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content[0].idProcessoPai", Matchers.equalTo(3000)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void updateProcesso() throws Exception {

        String processo0000002022017JTROOO1 = IOUtils.toString(this.getClass().getResourceAsStream("/processo/0000002022017JTROOO1.json"));
        
        this.mvc.perform(
                put("/processos/2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(processo0000002022017JTROOO1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.equalTo(2)))
                .andExpect(jsonPath("$.descricao", Matchers.equalTo("Cliente normal")));
        
        this.mvc.perform(
                get("/processos/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.pastaFisicaCliente", Matchers.equalTo("Pas00233")))                
                .andExpect(jsonPath("$.situacao.id", Matchers.equalTo(3)))
                .andExpect(jsonPath("$.responsaveis", Matchers.hasSize(1)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void deleteProcesso() throws Exception {

        this.mvc.perform(
                delete("/processos/11"))
                .andExpect(status().isOk());
        
        this.mvc.perform(
                get("/responsaveis/11"))
                .andExpect(status().isNotFound());
    }    
}
