package sajadv.controller.rest.processo;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.hamcrest.Matchers;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcessoControllerSearchTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPaginacaoPadrao() throws Exception {

        this.mvc.perform(
                get("/processos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(20)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findAllResponsaveisResumido() throws Exception {

        this.mvc.perform(
                get("/processos/resumido"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", Matchers.hasSize(44)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPaginacao50() throws Exception {

        this.mvc.perform(
                get("/processos").param("page", "0").param("size", "50"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(44)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorNomeResponsavel() throws Exception {

        this.mvc.perform(
                get("/processos").param("page", "0").param("size", "50").param("nomeResponsavel", "Souza"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(25)));
    }
    
    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorNPU() throws Exception {

        this.mvc.perform(
                get("/processos").param("npu", "0000001-01.2017.JTR.OOO1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(1)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorDataDistribuicao() throws Exception {

        String data = ZonedDateTime.now().format( DateTimeFormatter.ISO_LOCAL_DATE_TIME );

        this.mvc.perform(
                get("/processos").param("dataDistribuicaoInicial", data))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(20)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorSegredoJustica() throws Exception {

        this.mvc.perform(
                get("/processos").param("segredoJustica", "true"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(12)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorPastaFisicaCliente() throws Exception {

        this.mvc.perform(
                get("/processos").param("pastaFisicaCliente", "Pas01"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(10)));
    }

    @Test
    @Sql({"/delete_all.sql", "/processo/load_processos.sql"})
    public void findProcessosPorSituacao() throws Exception {

        this.mvc.perform(
                get("/processos").param("idSituacao", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(5)));
    }

}
