package sajadv.controller.rest.responsavel;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.hamcrest.Matchers;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResponsavelControllerCRUDTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }
    
    @Test
    @Sql("/delete_all.sql")
    public void createResponsavel() throws Exception {

        String joaoSerafim = IOUtils.toString(this.getClass().getResourceAsStream("/responsavel/joao_serafim.json"));
        
        this.mvc.perform(
                post("/responsaveis")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(joaoSerafim))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.notNullValue()))
                .andExpect(jsonPath("$.cpf", Matchers.equalTo("76144967362")));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void updateResponsavel() throws Exception {

        String manoelDalPonte = IOUtils.toString(this.getClass().getResourceAsStream("/responsavel/manoel_dal_ponte.json"));
        
        this.mvc.perform(
                put("/responsaveis/7")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(manoelDalPonte))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.equalTo(7)))
                .andExpect(jsonPath("$.nome", Matchers.equalTo("Manoel Dal Ponte")))
                .andExpect(jsonPath("$.email", Matchers.equalTo("manoeldalponte@dominio.com")));
        
        this.mvc.perform(
                get("/responsaveis/7"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", Matchers.equalTo("manoeldalponte@dominio.com")));
    }
    
    @Test    
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void deleteResponsavel() throws Exception {

        this.mvc.perform(
                delete("/responsaveis/10"))
                .andExpect(status().isOk());
        
        this.mvc.perform(
                get("/responsaveis/10"))
                .andExpect(status().isNotFound());
    }
    
}
