package sajadv.controller.rest.responsavel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import org.hamcrest.Matchers;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResponsavelControllerSearchTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPaginacaoPadrao() throws Exception {

        this.mvc.perform(
                get("/responsaveis"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(20)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findAllResponsaveisResumido() throws Exception {

        this.mvc.perform(
                get("/responsaveis/resumido"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", Matchers.hasSize(31)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPaginacao50() throws Exception {

        this.mvc.perform(
                get("/responsaveis").param("page", "0").param("size", "50"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(31)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorNome() throws Exception {

        this.mvc.perform(
                get("/responsaveis").param("nome", "Souza"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(4)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorCpf() throws Exception {

        this.mvc.perform(
                get("/responsaveis").param("cpf", "505.065.491-27"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(1)));
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorNPU() throws Exception {

        this.mvc.perform(
                get("/responsaveis").param("npu", "0000002-02.2017.JTR.OOO1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.content", Matchers.hasSize(2)));
    }
    
}
