package sajadv.domain.responsavel;

import javax.validation.ConstraintViolationException;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import sajadv.domain.CauseMatcher;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResponsavelRepositoryCRUDTests {

    @Autowired
    private ResponsavelRepository repository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Sql("/delete_all.sql")
    public void createResponsavel() {
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("54651328334");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);

        assertThat(marioAndrade.getId()).isNotNull();
        assertThat(marioAndrade.getDataCriacao()).isNotNull();
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelSemNome() throws Exception{
        //expectedException.expect(ConstraintViolationException.class);
        //expectedException.expectMessage("Validation failed");
        //expectedException.expectMessage("interpolatedMessage='não pode ser nulo', propertyPath=nome");
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='não pode ser nulo', propertyPath=nome"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setCpf("54651328334");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelComNomeGrande() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 0 e 150', propertyPath=nome"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade ahdaijsdlfjdjsfçljagçdjfgsdjsdçlgjsdglkjsdlgçjdsgskgjdçfglsdjgçsldfjgsdkgfjsçglksjdgçsdgjgjsdglksçgjsçgjsçlgkjsdglçjçalsjfaçlgjçalgjaçlfjhglgkjagljsdfg");
        marioAndrade.setCpf("54651328334");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelSemCpf() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='não pode ser nulo', propertyPath=cpf"));
                
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelComCpfInvalido() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='CPF inválido', propertyPath=cpf"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("54651328333");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelComCpfDuplicado() {
        expectedException.expectCause(
                CauseMatcher.to(org.hibernate.exception.ConstraintViolationException.class, "could not execute"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("17276624820");
        marioAndrade.setEmail("marioandrade@dominio.com");

        this.repository.save(marioAndrade);

        Responsavel joseDias = new Responsavel();
        joseDias.setNome("José Dias");
        joseDias.setCpf("17276624820");
        joseDias.setEmail("josedias@dominio.com");

        this.repository.save(joseDias);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelSemEmail() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='não pode ser nulo', propertyPath=email"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("40830392564");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelComEmailGrande() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 0 e 400', propertyPath=email"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("40830392564");
        marioAndrade.setEmail("marioandradeahkldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgmari"
                + "oandradeahkldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghmarioandrads"
                + "eahkldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgmarioandradeahks"
                + "ldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgmarioandradeahkldfhs"
                + "aklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgmarioandradeahkldfhaklfh"
                + "marioandradeahkldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgdfdfs"
                + "marioandradeahkldfhaklfhslksdhglkfhlahgsklfgjhsdglksfhgkdjghdsgdsg@dominio.com");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void createResponsavelComEmailInvalido() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='Não é um endereço de e-mail', propertyPath=email"));
        
        Responsavel marioAndrade = new Responsavel();
        marioAndrade.setNome("Mario de Andrade");
        marioAndrade.setCpf("40830392564");
        marioAndrade.setEmail("marioandrade");

        this.repository.save(marioAndrade);
    }

    @Test()
    @Sql("/delete_all.sql")
    public void updateResponsavel() {
        Responsavel mariaLago = new Responsavel();
        mariaLago.setNome("Maria Lago Costa");
        mariaLago.setCpf("54285284430");
        mariaLago.setEmail("marialago@dominio.com");

        this.repository.save(mariaLago);

        Responsavel mariaLagoPersisted = this.repository.findOne(mariaLago.getId());
        mariaLagoPersisted.setNome("Maria Lago da Costa");
        mariaLagoPersisted.setEmail("mariacosta@dominio.com");
        mariaLagoPersisted.setFoto("data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/"
                + "Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjog"
                + "Ni4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cu"
                + "dzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3Zn"
                + "IiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgi"
                + "IHk9IjBweCIgdmlld0JveD0iMCAwIDIwNi42NzYgMjA2LjY3NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjA2LjY3"
                + "NiAyMDYuNjc2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjEyOHB4IiBoZWlnaHQ9IjEyOHB4Ij4KPGc+Cgk8cGF0aCBzdHls"
                + "ZT0iZmlsbDojMzMwQzAwOyIgZD0iTTEwMy40ODUsNDMuNTE0djE2MC4wMDRjMCwwLTkwLjgzLDkuNDEyLTkwLjgzLTM0Ljg2NmMwLDAs"
                + "MjQuMzk3LDMuMDc1LDE5LjQwMi0xNy4yNzMgICBjLTYuMzMyLTI1Ljc5Ni0yLjc5LTY3LjU3NCwwLTkwLjQwOEMzNC44NDcsMzguMTM2"
                + "LDEwMy40ODUsNDMuNTE0LDEwMy40ODUsNDMuNTE0eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6IzMzMEMwMDsiIGQ9Ik0xMDMuMTkxLDQz"
                + "LjUxNHYxNjAuMDA0YzAsMCw5MC44Myw5LjQxMiw5MC44My0zNC44NjZjMCwwLTI0LjM5NywzLjA3NS0xOS40MDItMTcuMjczICAgYzYu"
                + "MzMyLTI1Ljc5NiwyLjc5LTY3LjU3NCwwLTkwLjQwOEMxNzEuODI4LDM4LjEzNiwxMDMuMTkxLDQzLjUxNCwxMDMuMTkxLDQzLjUxNHoi"
                + "Lz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMwMEI0OTc7IiBkPSJNMTA1LjA3LDE3OS43MjN2LTIzLjMxMWMwLDAsMzcuMDAyLTEuMTIxLDM3"
                + "LjAwMiwyMS4yNzRMMTA1LjA3LDE3OS43MjN6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMDBCNDk3OyIgZD0iTTEwMS45LDE3OS43MjN2"
                + "LTIzLjMxMWMwLDAtMzcuMDAyLTEuMTIxLTM3LjAwMiwyMS4yNzRMMTAxLjksMTc5LjcyM3oiLz4KCTxnPgoJCTxnPgoJCQk8cmVjdCB4"
                + "PSI4OC43ODgiIHk9IjE1Ni40MSIgc3R5bGU9ImZpbGw6I0ZEQ0M5QjsiIHdpZHRoPSIyOS4zOTUiIGhlaWdodD0iMzIuOTIzIi8+CgkJ"
                + "CTxwYXRoIHN0eWxlPSJmaWxsOiNGQ0JDODU7IiBkPSJNODguNzg4LDE2MC4yODhjMCwwLDEyLjM2NCw3Ljg3NCwyOS4zOTUsNi4wNTF2"
                + "LTkuOTI5SDg4Ljc4OFYxNjAuMjg4eiIvPgoJCQk8ZWxsaXBzZSBzdHlsZT0iZmlsbDojRkNCQzg1OyIgY3g9IjQ2LjQiIGN5PSIxMDUu"
                + "MzYiIHJ4PSIxNC4zNDMiIHJ5PSIxNi4zNjQiLz4KCQkJPGVsbGlwc2Ugc3R5bGU9ImZpbGw6I0ZDQkM4NTsiIGN4PSIxNjAuNTY4IiBj"
                + "eT0iMTA1LjM2IiByeD0iMTQuMzQzIiByeT0iMTYuMzY0Ii8+CgkJCTxwYXRoIHN0eWxlPSJmaWxsOiNGRENDOUI7IiBkPSJNMTYwLjUx"
                + "LDc0LjEwNGMwLTI0LjEwNC0xNy42MzctNDEuNzQtNTcuMDI1LTQxLjc0Yy0zOS4zOSwwLTU3LjAyNiwxNy42MzctNTcuMDI2LDQxLjc0"
                + "ICAgICBjMCwyNC4xMDQtNC4xMTUsODcuNTk3LDU3LjAyNiw4Ny41OTdDMTY0LjYyNiwxNjEuNzAxLDE2MC41MSw5OC4yMDksMTYwLjUx"
                + "LDc0LjEwNHoiLz4KCQkJPGc+CgkJCQk8Zz4KCQkJCQk8ZWxsaXBzZSBzdHlsZT0iZmlsbDojM0IyNTE5OyIgY3g9Ijc1LjcwOSIgY3k9"
                + "IjEwMS40NDIiIHJ4PSI2LjE3MyIgcnk9IjYuNzYxIi8+CgkJCQkJPGNpcmNsZSBzdHlsZT0iZmlsbDojRkZGRkZGOyIgY3g9IjczLjAx"
                + "OCIgY3k9Ijk4LjQ1NyIgcj0iMS44NDYiLz4KCQkJCTwvZz4KCQkJCTxwYXRoIHN0eWxlPSJmaWxsOiM2ODRCMzc7IiBkPSJNNjUuODM0"
                + "LDg2Ljg2NWMyLjkxOSwxLjExNyw3LjYwNi0zLjc5OSwxOC4zMzYsMC40NzhjMS45NTUsMC43OCwzLjEyMy02LjY2OC04LjM3OC02LjY2"
                + "OCAgICAgIEM2NS44MzQsODAuNjc1LDY0LjA3LDg2LjE4OSw2NS44MzQsODYuODY1eiIvPgoJCQkJPGc+CgkJCQkJPGVsbGlwc2Ugc3R5"
                + "bGU9ImZpbGw6IzNCMjUxOTsiIGN4PSIxMzIuMDIzIiBjeT0iMTAxLjQ0MiIgcng9IjYuMTczIiByeT0iNi43NjEiLz4KCQkJCQk8Y2ly"
                + "Y2xlIHN0eWxlPSJmaWxsOiNGRkZGRkY7IiBjeD0iMTI5LjMzMiIgY3k9Ijk4LjQ1NyIgcj0iMS44NDciLz4KCQkJCTwvZz4KCQkJCTxw"
                + "YXRoIHN0eWxlPSJmaWxsOiM2ODRCMzc7IiBkPSJNMTQxLjEzNSw4Ni44NjVjLTIuOTE5LDEuMTE3LTcuNjA1LTMuNzk5LTE4LjMzNSww"
                + "LjQ3OGMtMS45NTUsMC43OC0zLjEyMy02LjY2OCw4LjM3OC02LjY2OCAgICAgIEMxNDEuMTM1LDgwLjY3NSwxNDIuODk5LDg2LjE4OSwx"
                + "NDEuMTM1LDg2Ljg2NXoiLz4KCQkJPC9nPgoJCQk8cGF0aCBzdHlsZT0iZmlsbDojRkNCQzg1OyIgZD0iTTEwMy40ODUsMTI1LjIyM2Mt"
                + "Ni4xLDAtOS43NzQtNC41NTctOS43NzQtMi4zNTJjMCwyLjIwNCwxLjc2NCw2LjM5NCw5Ljc3NCw2LjM5NCAgICAgYzguMDEsMCw5Ljc3"
                + "My00LjE4OSw5Ljc3My02LjM5NEMxMTMuMjU4LDEyMC42NjYsMTA5LjU4NCwxMjUuMjIzLDEwMy40ODUsMTI1LjIyM3oiLz4KCQkJPHBh"
                + "dGggc3R5bGU9ImZpbGw6I0ZDQkM4NTsiIGQ9Ik0xMDMuNDg1LDE0NS40OTNjLTIuMTEsMC0zLjM4MS0xLjU3Ni0zLjM4MS0wLjgxM3Mw"
                + "LjYxLDIuMjEyLDMuMzgxLDIuMjEyczMuMzgtMS40NDksMy4zOC0yLjIxMiAgICAgUzEwNS41OTQsMTQ1LjQ5MywxMDMuNDg1LDE0NS40"
                + "OTN6Ii8+CgkJCTxwYXRoIHN0eWxlPSJmaWxsOiNGNzk0NUU7IiBkPSJNMTAzLjQ4NSwxMzkuODc2Yy05LjY2OSwwLTE1LjQ5My0zLjkz"
                + "OC0xNS40OTMtMi45NGMwLDAuOTk3LDIuNzk2LDQuOTI0LDE1LjQ5Myw0LjkyNCAgICAgYzEyLjY5NywwLDE1LjQ5My0zLjkyNywxNS40"
                + "OTMtNC45MjRDMTE4Ljk3OCwxMzUuOTM5LDExMy4xNTMsMTM5Ljg3NiwxMDMuNDg1LDEzOS44NzZ6Ii8+CgkJPC9nPgoJCTxjaXJjbGUg"
                + "c3R5bGU9ImZpbGw6I0Y5QUE4RDsiIGN4PSI2Ny43NzEiIGN5PSIxMjMuMDQ4IiByPSI4LjM3NyIvPgoJCTxjaXJjbGUgc3R5bGU9ImZp"
                + "bGw6I0Y5QUE4RDsiIGN4PSIxNDAuMjI5IiBjeT0iMTIzLjA0OCIgcj0iOC4zNzciLz4KCTwvZz4KCTxwYXRoIHN0eWxlPSJmaWxsOiM4"
                + "RkQ2RTc7IiBkPSJNMTAzLjQ4NSwxNjkuNjM4djM3LjAzOEgzOS4xMTJDMzkuMTEyLDE5MS42ODUsNjkuNjgxLDE2OS42MzgsMTAzLjQ4"
                + "NSwxNjkuNjM4eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6IzhGRDZFNzsiIGQ9Ik0xMDMuNDg1LDE2OS42Mzh2MzcuMDM4aDY0LjM3MkMx"
                + "NjcuODU3LDE5MS42ODUsMTM3LjI4OSwxNjkuNjM4LDEwMy40ODUsMTY5LjYzOHoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNGNkIxQ0Y7"
                + "IiBkPSJNMTAzLjQ4NSwyMDQuOTk5di0zNS4zNjFjLTkuNDE3LDAtMTguNTUxLDEuNzUyLTI2Ljg3NCw0LjU0NEwxMDMuNDg1LDIwNC45"
                + "OTl6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRjZCMUNGOyIgZD0iTTEwMy40ODUsMjA0Ljk5OXYtMzUuMzYxYzkuNDE2LDAsMTguNTUs"
                + "MS43NTIsMjYuODc0LDQuNTQ0TDEwMy40ODUsMjA0Ljk5OXoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNGMjgzQjQ7IiBkPSJNMTAzLjQ4"
                + "NSwxNjkuNjM4Yy02Ljg0OSwwLTEzLjU0NSwwLjkzOS0xOS45LDIuNTA3YzAuOTA1LDUuNTAxLDQuNjIsMTYuNzg3LDE5LjksMTYuNzg3"
                + "ICAgYzE1LjI3OSwwLDE4Ljk5NC0xMS4yODYsMTkuODk5LTE2Ljc4N0MxMTcuMDMsMTcwLjU3NiwxMTAuMzM0LDE2OS42MzgsMTAzLjQ4"
                + "NSwxNjkuNjM4eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0ZEQ0M5QjsiIGQ9Ik04OC43ODgsMTcwLjkyYzAsMC0wLjAwMSwxMy4yMzks"
                + "MTQuNjk3LDEzLjIzOWMxNC42OTcsMCwxNC42OTctMTMuMjM5LDE0LjY5Ny0xMy4yMzkgICBTMTAyLjkzMywxNjUuMzY4LDg4Ljc4OCwx"
                + "NzAuOTJ6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojM0FCREFBOyIgZD0iTTgwLjk5MiwxNzIuNDAxYzAuNDQtMi44NjEtMTcuMTE5LTAu"
                + "NzUyLTE2Ljk3Niw3LjMyMWMwLjE0NSw4LjA3MywxOS40MDEsMTQuMjg0LDI0LjY5MiwxNy4yMjQgICBjNS4yOTEsMi45MzksMTQuNjk3"
                + "LDkuNzI5LDE0LjY5Nyw5LjcyOXMxLjc2NC02LjgyLTYuNDY3LTEyLjA4MkM4OC43MDksMTg5LjMzMyw4MC40MDQsMTc2LjIyMyw4MC45"
                + "OTIsMTcyLjQwMXoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMzQUJEQUE7IiBkPSJNMTI1Ljc2MywxNzIuNDAxYy0wLjQ0LTIuODYxLDE3"
                + "LjExOS0wLjc1MiwxNi45NzYsNy4zMjFjLTAuMTQ1LDguMDczLTE5LjQwMSwxNC4yODQtMjQuNjkyLDE3LjIyNCAgIGMtNS4yOTEsMi45"
                + "MzktMTQuNjk3LDkuNzI5LTE0LjY5Nyw5LjcyOXMtMS43NjQtNi44Miw2LjQ2Ny0xMi4wODJDMTE4LjA0NiwxODkuMzMzLDEyNi4zNTEs"
                + "MTc2LjIyMywxMjUuNzYzLDE3Mi40MDF6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojNDAxOTAwOyIgZD0iTTEwMy40ODUsMEMzNy4zNDYs"
                + "MCwyOC41MjgsNTcuNzQ3LDI4LjUyOCwxMDAuMzAzYzAsNDIuNTU3LDIyLjAzMSw2Ny4yNywzOS4yNDMsNzQuMTcyICAgYzAsMC0yNy40"
                + "ODUtNDguNzA4LTEzLjk2NC05My44YzExLjA2My0zNi44OTQsNDkuNjc4LTI2LDQ5LjY3OC0yNnMzOC42MTMtMTAuODk1LDQ5LjY3Nywy"
                + "NmMxMy41MjEsNDUuMDkyLTEzLjk2NCw5My44LTEzLjk2NCw5My44ICAgYzE3LjIxMy02LjkwMiwzOS4yNDQtMzEuNjE1LDM5LjI0NC03"
                + "NC4xNzJDMTc4LjQ0Miw1Ny43NDcsMTY5LjYyNCwwLDEwMy40ODUsMHoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiM0MDE5MDA7IiBkPSJN"
                + "MTI1Ljg1LDY0Ljg2NGMwLDAtMjQuODI0LDEyLjQ0NC00Ni43NDYsOS45NDZjLTIxLjkyMS0yLjQ5OS0zMC4zNDEsMjAuMjI4LTI5LjAw"
                + "MywzNS4yNjkgICBsLTcuMzE2LTMzLjQ3NmwxMy4yMjgtMjYuNDg1bDM3LjY5OC0xOS40bDM1LjYyMSwxLjY0NmwxNi44OTMsMTQuNjY4"
                + "bDE0LjM0NCwxNy44MzJsMTAuMDgzLDExLjczOWwtNC44NTEsMzYuMTI2bC0xMC4yNjgsMTIuMzQ5ICAgQzE1NS41MzQsMTI1LjA3OCwx"
                + "NjQuODI0LDc4LjcyOCwxMjUuODUsNjQuODY0eiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8"
                + "L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjxn"
                + "Pgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=");

        this.repository.save(mariaLagoPersisted);

        Responsavel mariaLagoUpdated = this.repository.findOne(mariaLago.getId());

        assertThat(mariaLagoUpdated.getNome()).isEqualTo("Maria Lago da Costa");
        assertThat(mariaLagoUpdated.getEmail()).isEqualTo("mariacosta@dominio.com");
    }

    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void deleteResponsavel() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Responsavel> responsaveis = this.repository.find("", "73368328735", null, pageRequest);

        assertThat(responsaveis.getTotalElements()).isEqualTo(1L);

        Responsavel marioSilva = responsaveis.getContent().get(0);
        assertThat(marioSilva.getNome()).isEqualTo("Mario Silva");

        this.repository.delete(marioSilva);

        assertThat(this.repository.findOne(marioSilva.getId())).isNull();
    }
}
