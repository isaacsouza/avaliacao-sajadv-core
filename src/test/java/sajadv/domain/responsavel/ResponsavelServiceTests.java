package sajadv.domain.responsavel;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResponsavelServiceTests {

    @Autowired
    private ResponsavelService service;

    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorCpf() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("Souza", "505.065.491-27", null);
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(1L);
        
        Responsavel isaiasSouza = responsaveis.getContent().get(0);
        assertThat(isaiasSouza.getNumeroUnificadoProcessos().size()).isEqualTo(2);
        assertThat(isaiasSouza.getNumeroUnificadoProcessos()).contains("0000001012017JTROOO1","0000002022017JTROOO1");
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsavelPorNumeroProcessoUnificado() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("", null, "0000001012017JTROOO1");
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(1L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsavelPorNumeroProcessoUnificadoFormatado() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("", null, "0000001-01.2017.JTR.OOO1");
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(1L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsavelPorNumeroProcessoUnificadoInexistente() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("", null, "0000005052017JTROOO1");
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(0L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisFiltroVazio() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("", "", "");
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(31L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorNumeroProcessoUnificado() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ResponsavelFilter filter = new ResponsavelFilter("", null, "0000002022017JTROOO1");
        Page<Responsavel> responsaveis = this.service.find(filter, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(2L);
        
    }
}
