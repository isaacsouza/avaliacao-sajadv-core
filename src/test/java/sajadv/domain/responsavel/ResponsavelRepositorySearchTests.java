package sajadv.domain.responsavel;

import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResponsavelRepositorySearchTests {

    @Autowired
    private ResponsavelRepository repository;

    @Test    
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findAllResponsaveisPaginados() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Responsavel> responsaveis = this.repository.findAll(pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(31L);
        assertThat(responsaveis.getTotalPages()).isEqualTo(4);
    }

    @Test    
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findAllResponsaveis() {
        
        List<Responsavel> responsaveis = this.repository.findAllResponsaveisResumido();
        assertThat(responsaveis).hasSize(31);
    }
    
    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisDadosNulo() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Responsavel> responsaveis = this.repository.find(null, null, null, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(31L);
    }

    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorNome() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Responsavel> responsaveis = this.repository.find("Souza", null, null, pageRequest);

        assertThat(responsaveis.getTotalElements()).isEqualTo(4L);
        assertThat(responsaveis.getContent()).filteredOn("nome", "Isaias Souza");
        assertThat(responsaveis.getContent()).filteredOn("nome", "Gabriela Souza");
        assertThat(responsaveis.getContent()).filteredOn("nome", "João de Souza Prado");
        assertThat(responsaveis.getContent()).filteredOn("nome", "Souza Santos");

    }

    @Test
    @Sql({"/delete_all.sql","/responsavel/load_responsaveis.sql"})
    public void findResponsaveisPorCpf() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Responsavel> responsaveis = this.repository.find(null, "50506549127", null, pageRequest);
        assertThat(responsaveis.getTotalElements()).isEqualTo(1L);

        Responsavel isaiasSouza = responsaveis.getContent().get(0);
        assertThat(isaiasSouza.getNome()).isEqualTo("Isaias Souza");
        assertThat(isaiasSouza.getEmail()).isEqualTo("isaiassouza@dominio.com");
    }

}
