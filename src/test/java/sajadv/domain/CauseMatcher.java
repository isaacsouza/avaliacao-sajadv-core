/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sajadv.domain;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 *
 * @author isaacsouza
 */
public class CauseMatcher extends TypeSafeMatcher<Throwable> {

    private final Class<? extends Throwable> type;
    private final String expectedMessage;

    private CauseMatcher(Class<? extends Throwable> type, String expectedMessage) {
        this.type = type;
        this.expectedMessage = expectedMessage;
    }

    public static CauseMatcher to(Class<? extends Throwable> type, String expectedMessage){
        return new CauseMatcher(type, expectedMessage);
    }
    
    @Override
    protected boolean matchesSafely(Throwable item) {
        boolean throwableExpected =
                ((item.getClass().isAssignableFrom(type)
                || item.getCause().getClass().isAssignableFrom(type))
                &&
                ( item.getMessage().contains(expectedMessage))
                || item.getCause().getMessage().contains(expectedMessage));
        
        return throwableExpected;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("expects type ")
                .appendValue(type)
                .appendText(" and a message ")
                .appendValue(expectedMessage);
    }
}