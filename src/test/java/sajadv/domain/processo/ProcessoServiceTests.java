package sajadv.domain.processo;

import java.time.LocalDate;
import sajadv.domain.responsavel.*;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcessoServiceTests {

    @Autowired
    private ProcessoService service;

    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessos() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ProcessoFilter filter = new ProcessoFilter(null, null, null, null, null, null, null);
        Page<Processo> processos = this.service.find(filter, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(44L);
        assertThat(processos.getTotalPages()).isEqualTo(5);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosFiltroVazio() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ProcessoFilter filter = new ProcessoFilter("", null, null, null, "", null, "");
        Page<Processo> processos = this.service.find(filter, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(44L);
        assertThat(processos.getTotalPages()).isEqualTo(5);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessoPorNPUFormatado() {
        PageRequest pageRequest = new PageRequest(0, 10);
        ProcessoFilter filter = new ProcessoFilter("0000004-04.2017.JTR.OOO1", null, null, null, "", null, "");
        Page<Processo> processos = this.service.find(filter, pageRequest);
        assertThat(processos.getTotalElements()).isEqualTo(1L);
        
        Processo isaiasSouza = processos.getContent().get(0);
        assertThat(isaiasSouza.getPastaFisicaCliente()).isEqualTo("Pas004");
        assertThat(isaiasSouza.getDescricao()).isEqualTo("Cliente está sob custódia");
    }
}
