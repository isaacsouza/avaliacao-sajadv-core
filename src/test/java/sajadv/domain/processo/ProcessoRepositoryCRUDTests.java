package sajadv.domain.processo;

import java.time.LocalDate;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import static org.assertj.core.api.Assertions.assertThat;
import org.assertj.core.util.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import sajadv.domain.CauseMatcher;
import sajadv.domain.responsavel.Responsavel;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcessoRepositoryCRUDTests {

    @Autowired
    private ProcessoRepository repository;
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
        
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcesso() {
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0000101012017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
        
        assertThat(processo.getId()).isNotNull();
        assertThat(processo.getDataCriacao()).isNotNull();
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoComProcessoPai() {
        Processo processoPai = new Processo();
        processoPai.setNumeroProcessoUnificado("0001101012017JTR1000");
        processoPai.setDataDistribuicao(LocalDate.now());
        processoPai.setPastaFisicaCliente("Pas101");
        processoPai.setSegredoJustica(Boolean.FALSE);
        processoPai.setDescricao("Cliente desaparecido");
        processoPai.setSituacao(new Situacao(1L));
        processoPai.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processoPai);
        
        assertThat(processoPai.getId()).isNotNull();
        assertThat(processoPai.getDataCriacao()).isNotNull();
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0001102022017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(2L)));
        processo.setIdProcessoPai(processoPai.getId());
        
        this.repository.save(processo);
        
        assertThat(processo.getId()).isNotNull();
        assertThat(processo.getDataCriacao()).isNotNull();
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoSemNPU() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='não pode ser nulo', propertyPath=numeroProcessoUnificado"));
        
        Processo processo = new Processo();
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoNPUGrande() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 0 e 20', propertyPath=numeroProcessoUnificado"));
        
        Processo processo = new Processo();        
        processo.setNumeroProcessoUnificado("0001102022017JTR10002232132333");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
        
        assertThat(processo.getId()).isNotNull();
        assertThat(processo.getDataCriacao()).isNotNull();
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoNPUDuplicado() {
        expectedException.expectCause(
                CauseMatcher.to(org.hibernate.exception.ConstraintViolationException.class, "could not execute"));
        
        Processo processo = new Processo();        
        processo.setNumeroProcessoUnificado("0001103022017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
        
        Processo processoDuplicado = new Processo();        
        processoDuplicado.setNumeroProcessoUnificado("0001103022017JTR1000");
        processoDuplicado.setDataDistribuicao(LocalDate.now());
        processoDuplicado.setPastaFisicaCliente("Pas102");
        processoDuplicado.setSegredoJustica(Boolean.TRUE);
        processoDuplicado.setDescricao("Cliente duplicado");
        processoDuplicado.setSituacao(new Situacao(1L));
        processoDuplicado.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processoDuplicado);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoDataDistribuicaoInvalida() {
        expectedException.expect(ValidationException.class);
        expectedException.expectMessage("Data de distribuição deve ser menor ou igual a data atual.");
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0000109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now().plusDays(1));
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoPastaFisicaClienteGrande() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 0 e 50', propertyPath=pastaFisicaCliente"));
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0000109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101"
                + "Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101"
                + "Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoDescricaoGrande() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 0 e 1000', propertyPath=descricao"));
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0001109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Caros amigos, a execução dos pontos do programa nos obriga à análise do impacto na agilidade "
                + "decisória. As experiências acumuladas demonstram que o acompanhamento das preferências de consumo cumpre "
                + "um papel essencial na formulação do sistema de formação de quadros que corresponde às necessidades. Assim "
                + "mesmo, a estrutura atual da organização apresenta tendências no sentido de aprovar a manutenção do fluxo "
                + "de informações. Evidentemente, a hegemonia do ambiente político representa uma abertura para a melhoria "
                + "dos métodos utilizados na avaliação de resultados. Todas estas questões, devidamente ponderadas, levantam "
                + "dúvidas sobre se a percepção das dificuldades maximiza as possibilidades por conta dos relacionamentos "
                + "verticais entre as hierarquias. O incentivo ao avanço tecnológico, assim como a competitividade nas transações "
                + "comerciais assume importantes posições no estabelecimento das direções preferenciais no sentido do progresso. "
                + "Ainda assim, existem dúvidas a respeito de como a constante divulgação das informações facilita a criação do "
                + "levantamento das variáveis envolvidas. Por outro lado, a consolidação das estruturas estende o alcance e a "
                + "importância dos índices pretendidos. Todavia, a consulta aos diversos militantes oferece uma interessante "
                + "oportunidade para verificação das condições inegavelmente apropriadas. No mundo atual, o início da atividade "
                + "geral de formação de atitudes garante a contribuição de um grupo importante na determinação das diretrizes de "
                + "desenvolvimento para o futuro. ");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoSemSituacao() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='não pode ser nulo', propertyPath=situacao"));
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0003109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(processo);        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoSemResponsavel() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 1 e 3', propertyPath=responsaveis"));
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0007109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        
        this.repository.save(processo);        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_responsaveis.sql"})
    public void createProcessoMaisDeTresResponsavel() {
        expectedException.expectCause(
                CauseMatcher.to(ConstraintViolationException.class, "interpolatedMessage='tamanho deve estar entre 1 e 3', propertyPath=responsaveis"));
        
        Processo processo = new Processo();
        processo.setNumeroProcessoUnificado("0007109092017JTR1000");
        processo.setDataDistribuicao(LocalDate.now());
        processo.setPastaFisicaCliente("Pas101");
        processo.setSegredoJustica(Boolean.FALSE);
        processo.setDescricao("Cliente desaparecido");
        processo.setSituacao(new Situacao(1L));
        processo.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L), new Responsavel(4L), new Responsavel(3L), new Responsavel(2L)));
        
        this.repository.save(processo);        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void deleteProcessoFinalizado() {
        expectedException.expect(ValidationException.class);
        expectedException.expectMessage("Processo com situação finalizada não pode ser removido.");
        
        Processo processo = this.repository.findOne(38L);
        this.repository.delete(processo);        
    }
}
