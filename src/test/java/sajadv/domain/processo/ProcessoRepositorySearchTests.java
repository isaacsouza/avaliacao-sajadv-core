package sajadv.domain.processo;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.assertj.core.util.Sets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import sajadv.domain.responsavel.Responsavel;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProcessoRepositorySearchTests {

    @Autowired
    private ProcessoRepository repository;
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessos() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find(null, null, null, null, null, null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(44L);
        assertThat(processos.getTotalPages()).isEqualTo(5);
    }
    
    @Test    
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findAllProcessosResumido() {
        
        List<Processo> processos = this.repository.findAllProcessosResumido();
        assertThat(processos).hasSize(44);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosPorNumeroProcessoUnificado() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find("0000004042017JTROOO1", null, null, null, null, null, null, pageRequest);
        assertThat(processos.getTotalElements()).isEqualTo(1L);
        
        Processo isaiasSouza = processos.getContent().get(0);
        assertThat(isaiasSouza.getPastaFisicaCliente()).isEqualTo("Pas004");
        assertThat(isaiasSouza.getDescricao()).isEqualTo("Cliente está sob custódia");

    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosPorSegredoJustica() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find(null, null, null, true, null, null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(12L);
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosPorPastaFisicaCliente() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find(null, null, null, null, "Pas005", null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(1L);
        
        Processo isaiasSouza = processos.getContent().get(0);
        assertThat(isaiasSouza.getNumeroProcessoUnificado()).isEqualTo("0000005052017JTROOO1");
        assertThat(isaiasSouza.getSituacao().getNome()).isEqualTo("Desmembrado");

    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosPorPartePastaFisicaCliente() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find(null, null, null, null, "Pas03", null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(10L);        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos.sql"})
    public void findProcessosPorIdSituacao() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<Processo> processos = this.repository.find(null, null, null, null, null, 1L, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(26L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos_simplificado.sql"})
    public void findProcessosPorDataDistribuicaoInicial() {
        insertProcessosDataDiferentes();
        PageRequest pageRequest = new PageRequest(0, 10);
        LocalDate dataInical = LocalDate.now().minusDays(1);
        Page<Processo> processos = this.repository.find(null, dataInical, null, null, null, null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(9L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos_simplificado.sql"})
    public void findProcessosPorDataDistribuicaoFinal() {
        insertProcessosDataDiferentes();
        PageRequest pageRequest = new PageRequest(0, 10);
        LocalDate dataFinal = LocalDate.now().minusDays(1);
        Page<Processo> processos = this.repository.find(null, null, dataFinal, null, null, null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(5L);
        
    }
    
    @Test
    @Sql({"/delete_all.sql","/processo/load_processos_simplificado.sql"})
    public void findProcessosPorDataDistribuicaoPeriodo() {
        insertProcessosDataDiferentes();
        PageRequest pageRequest = new PageRequest(0, 10);
        LocalDate dataInicial = LocalDate.now().minusDays(3);
        LocalDate dataFinal = LocalDate.now().minusDays(1);
        Page<Processo> processos = this.repository.find(null, dataInicial, dataFinal, null, null, null, null, pageRequest);
        
        assertThat(processos.getTotalElements()).isEqualTo(2L);
        
    }
    
    public void insertProcessosDataDiferentes(){
        Processo processo1 = new Processo(null, "9001103022017JTR1000", LocalDate.now().minusDays(2));       
        processo1.setSituacao(new Situacao(1L));
        processo1.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        Processo processo2 = new Processo(null, "9001203022017JTR1000", LocalDate.now().minusDays(2));       
        processo2.setSituacao(new Situacao(1L));
        processo2.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        Processo processo3 = new Processo(null, "9001303022017JTR1000", LocalDate.now().minusDays(5));       
        processo3.setSituacao(new Situacao(1L));
        processo3.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        Processo processo4 = new Processo(null, "9001403022017JTR1000", LocalDate.now().minusDays(5));       
        processo4.setSituacao(new Situacao(1L));
        processo4.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        Processo processo5 = new Processo(null, "9001503022017JTR1000", LocalDate.now().minusDays(5));       
        processo5.setSituacao(new Situacao(1L));
        processo5.setResponsaveis(Sets.newLinkedHashSet(new Responsavel(1L)));
        
        this.repository.save(Arrays.asList(processo1, processo2, processo3, processo4, processo5));
    }
}
