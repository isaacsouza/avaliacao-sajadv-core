package sajadv.domain.processo;

import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author isaac.souza
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SituacaoServiceTests {

    @Autowired
    private SituacaoService service;

    @Test
    public void findProcessos() {
        List<Situacao> situacoes = this.service.findAll();
        
        assertThat(situacoes).hasSize(5);
    }
    
}
