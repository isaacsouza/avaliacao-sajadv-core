package sajadv.domain;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Utilitário interno de data.
 * @author isaacsouza
 */
public class DateUtils {
    
    public static Date convertToDate(LocalDate localDate){
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
