package sajadv.domain.responsavel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author isaac
 */
@Service
public class ResponsavelService {
    
    @Autowired
    ResponsavelRepository repository;
    
    public Page<Responsavel> find(ResponsavelFilter filter, Pageable pageable){
        
        return repository.find(filter.getNome(), filter.getCpf(), filter.getNumeroProcessoUnificado(), pageable);
    }
}
