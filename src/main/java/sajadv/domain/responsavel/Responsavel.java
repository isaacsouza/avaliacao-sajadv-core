package sajadv.domain.responsavel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.br.CPF;
import sajadv.domain.processo.Processo;

/**
 *
 * @author isaacsouza
 */
@Entity
@SequenceGenerator(name = "responsavelSeq", sequenceName = "RESPONSAVEL_SEQ", allocationSize = 1)
public class Responsavel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "responsavelSeq")
    private Long id;

    @Column()
    @NotNull
    @Size(max = 150)
    private String nome;

    @Column()
    @NaturalId
    @NotNull
    @CPF
    private String cpf;

    @Column(nullable = false)
    @NotNull
    @Size(max = 400)
    @Email
    private String email;

    @Column(columnDefinition = "TEXT")
    private String foto;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "responsaveis")
    private Set<Processo> processos;
  
    @Column()
    @NotNull
    private LocalDateTime dataCriacao;
    
    @Transient
    private Set<String> numeroUnificadoProcessos;

    public Responsavel() {
    }

    public Responsavel(Long id) {
        this.id = id;
    }

    public Responsavel(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }
        
    @PrePersist
    public void applyDataCricao(){
        dataCriacao = LocalDateTime.now();
    }
    
    public Set<String> getNumeroUnificadoProcessos() {
        if (numeroUnificadoProcessos == null && processos != null){
            numeroUnificadoProcessos = processos.stream().map(process -> process.getNumeroProcessoUnificado()).collect(Collectors.toSet());
        }
        
        return numeroUnificadoProcessos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Responsavel other = (Responsavel) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
