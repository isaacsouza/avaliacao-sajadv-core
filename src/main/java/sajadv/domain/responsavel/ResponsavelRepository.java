package sajadv.domain.responsavel;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author isaac.souza
 */

public interface ResponsavelRepository extends PagingAndSortingRepository<Responsavel, Long>{
    
    /**
     * Pesquisa principal para entidade responsavel.
     * @param name
     * @param socialNumber
     * @param processo
     * @param pageable
     * @return 
     */
    @Query("select distinct r from Responsavel r "
            + "left join r.processos p "
            + "where "
            + "(cast(:nome as string) is null or lower(r.nome) like lower(concat('%',cast(:nome as string), '%'))) "
            + "and (cast(:cpf as string) is null or r.cpf = :cpf) "
            + "and (cast(:numeroProcessoUnificado as string) is null or p.numeroProcessoUnificado = :numeroProcessoUnificado) "
            + "order by r.nome ")
    Page<Responsavel> find(@Param("nome") String name, 
            @Param("cpf") String socialNumber, 
            @Param("numeroProcessoUnificado") String processo, 
            Pageable pageable);
    
    @Query("select new Responsavel(r.id, r.nome) from Responsavel r order by r.nome ")
    List<Responsavel> findAllResponsaveisResumido();
}
