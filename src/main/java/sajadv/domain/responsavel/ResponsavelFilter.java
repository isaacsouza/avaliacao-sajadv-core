package sajadv.domain.responsavel;

import org.springframework.util.StringUtils;

/**
 * Classe auxiliar para agrupar os campos que são usados como filtro para 
 * pesquisas de responsaveis.
 * 
 * @author isaac
 */
public class ResponsavelFilter {
    
    private final String nome;
    private final String cpf;
    private final String numeroProcessoUnificado;

    public ResponsavelFilter(String nome, String cpf, String numeroProcessoUnificado) {
        this.nome = StringUtils.isEmpty(nome) ? null : nome;
        this.cpf = StringUtils.isEmpty(cpf) ? null : cpf.replaceAll("\\D+","") ;
        this.numeroProcessoUnificado = StringUtils.isEmpty(numeroProcessoUnificado) ? null : numeroProcessoUnificado.replaceAll("[\\W_]", "");
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {        
        return cpf;
    }

    public String getNumeroProcessoUnificado() {
        return numeroProcessoUnificado;
    }
}
