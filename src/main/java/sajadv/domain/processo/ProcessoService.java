package sajadv.domain.processo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author isaac
 */
@Service
public class ProcessoService {
    
    @Autowired
    ProcessoRepository repository;
    
    public Page<Processo> find(ProcessoFilter filter, Pageable pageable){
        
        return repository.find(filter.getNumeroProcessoUnificado(), 
                filter.getDataDistribuicaoInicial(), 
                filter.getDataDistribuicaoFinal(),
                filter.getSegredoJustica(), 
                filter.getPastaFisicaCliente(), 
                filter.getIdSituacao(),
                filter.getNomeResponsavel(), pageable);
    }
}
