package sajadv.domain.processo;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isaacsouza
 */
@Service
public class SituacaoService {
    
    @Autowired
    private EntityManager entityManager;
    
    public List<Situacao> findAll(){
        Query query = entityManager.createQuery("from Situacao");
        
        return query.getResultList();
    }
}
