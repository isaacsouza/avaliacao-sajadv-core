package sajadv.domain.processo;

import java.time.LocalDate;
import org.springframework.util.StringUtils;

/**
 * Classe auxiliar para agrupar os campos que são usados como filtro para 
 * pesquisas de processos.
 * @author isaac
 */
public class ProcessoFilter {
    
    private final String numeroProcessoUnificado;
    private final LocalDate dataDistribuicaoInicial;
    private final LocalDate dataDistribuicaoFinal;
    private final Boolean segredoJustica;
    private final String pastaFisicaCliente;
    private final Long idSituacao;
    private final String nomeResponsavel;

    public ProcessoFilter(
            String numeroProcessoUnificado,
            LocalDate dataDistribuicaoInicial,
            LocalDate dataDistribuicaoFinal,
            Boolean segredoJustica,
            String pastaFisicaCliente,
            Long idSituacao,
            String nomeResponsavel) {
        this.numeroProcessoUnificado = StringUtils.isEmpty(numeroProcessoUnificado) ? null : numeroProcessoUnificado.replaceAll("[\\W_]", "");
        this.dataDistribuicaoInicial = dataDistribuicaoInicial;
        this.dataDistribuicaoFinal = dataDistribuicaoFinal;
        this.segredoJustica = segredoJustica;
        this.pastaFisicaCliente = StringUtils.isEmpty(pastaFisicaCliente) ? null : pastaFisicaCliente;
        this.idSituacao = idSituacao;
        this.nomeResponsavel = StringUtils.isEmpty(nomeResponsavel) ? null : nomeResponsavel;
    }

    public String getNumeroProcessoUnificado() {
        return numeroProcessoUnificado;
    }

    public LocalDate getDataDistribuicaoInicial() {
        return dataDistribuicaoInicial;
    }

    public LocalDate getDataDistribuicaoFinal() {
        return dataDistribuicaoFinal;
    }

    public Boolean getSegredoJustica() {
        return segredoJustica;
    }

    public String getPastaFisicaCliente() {
        return pastaFisicaCliente;
    }

    public Long getIdSituacao() {
        return idSituacao;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    
}
