package sajadv.domain.processo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author isaacsouza
 */
@Entity
public class Situacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false)
    private Boolean finalizado;

    public Situacao() {
    }

    public Situacao(Long id) {
        this.id = id;
    }
    
    public Long getId() {
        return id;
    }
    
    public String getNome() {
        return this.nome;
    }

    public Boolean getFinalizado() {
        return finalizado;
    }

}
