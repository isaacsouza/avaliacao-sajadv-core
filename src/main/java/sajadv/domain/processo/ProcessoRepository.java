package sajadv.domain.processo;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author isaac.souza
 */
public interface ProcessoRepository extends PagingAndSortingRepository<Processo, Long>{
    
    /**
     * Pesquisa principal para entidade processo.
     * @param numeroProcessoUnificado
     * @param dataDistribuicaoInicial
     * @param dataDistribuicaoFinal
     * @param segredoJustica
     * @param pastaFisicaCliente
     * @param idSituacao
     * @param nomeResponsavel
     * @param pageable
     * @return 
     */
    @Query("select distinct p from Processo p "
            + "left join p.responsaveis r "
            + "where "
            + "(cast(:numeroProcessoUnificado as string) is null or p.numeroProcessoUnificado = :numeroProcessoUnificado) "
            + "and (cast(:dataDistribuicaoInicial as date) is null or p.dataDistribuicao >= :dataDistribuicaoInicial) "
            + "and (cast(:dataDistribuicaoFinal as date) is null or p.dataDistribuicao <= :dataDistribuicaoFinal) "
            + "and (cast(:segredoJustica as boolean) is null or p.segredoJustica = :segredoJustica) "
            + "and (cast(:pastaFisicaCliente as string) is null or lower(p.pastaFisicaCliente) like lower(concat('%',cast(:pastaFisicaCliente as string), '%'))) "
            + "and (cast(:idSituacao as long) is null or p.situacao.id = :idSituacao) "
            + "and (cast(:nomeResponsavel as string) is null or lower(r.nome) like lower(concat('%',cast(:nomeResponsavel as string), '%'))) "
            + "order by p.dataDistribuicao desc, p.numeroProcessoUnificado ")
    Page<Processo> find(@Param("numeroProcessoUnificado") String numeroProcessoUnificado,
            @Param("dataDistribuicaoInicial") LocalDate dataDistribuicaoInicial,
            @Param("dataDistribuicaoFinal") LocalDate dataDistribuicaoFinal,
            @Param("segredoJustica") Boolean segredoJustica,
            @Param("pastaFisicaCliente") String pastaFisicaCliente,
            @Param("idSituacao") Long idSituacao,
            @Param("nomeResponsavel") String nomeResponsavel,
            Pageable pageable);
    
    @Query("select new Processo(p.id, p.numeroProcessoUnificado, p.dataDistribuicao) from Processo p")
    List<Processo> findAllProcessosResumido();
}
