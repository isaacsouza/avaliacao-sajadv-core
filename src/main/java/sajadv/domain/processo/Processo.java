package sajadv.domain.processo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.NaturalId;
import sajadv.domain.responsavel.Responsavel;

/**
 *
 * @author isaacsouza
 */
@Entity
@SequenceGenerator(name = "processoSeq", sequenceName = "PROCESSO_SEQ", allocationSize = 1)
public class Processo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "processoSeq")
    private Long id;

    @Column()
    @NaturalId
    @NotNull
    @Size(max = 20)
    private String numeroProcessoUnificado;

    @Column(length = 1000)
    @Size(max = 1000)
    private String descricao;

    @Column()
    @Size(max = 50)
    private String pastaFisicaCliente;

    @Column()
    private Boolean segredoJustica;

    @ManyToOne(optional = false)
    @NotNull
    private Situacao situacao;

    @Column()
    private LocalDate dataDistribuicao;

    @Column()
    @NotNull
    private LocalDateTime dataCriacao;

    @NotNull
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "processoResponsaveis",
            joinColumns = {
                @JoinColumn(name = "processoId", nullable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "responsavelId", nullable = false)})
    @Size(min = 1, max = 3)
    private Set<Responsavel> responsaveis = new HashSet<Responsavel>();

    @Column(name = "idProcessoPai")
    public Long idProcessoPai;

    @OneToMany()
    @JoinColumn(name = "idProcessoPai")
    public Set<Processo> processosFilhos;

    public Processo() {
    }

    public Processo(Long id) {
        this.id = id;
    }

    public Processo(Long id, String numeroProcessoUnificado, LocalDate dataDistribuicao) {
        this.id = id;
        this.numeroProcessoUnificado = numeroProcessoUnificado;
        this.dataDistribuicao = dataDistribuicao;
    }
    
    @PrePersist
    @PreUpdate
    public void validateSave() {
        if (dataCriacao == null) {
            dataCriacao = LocalDateTime.now();
        }

        if (dataDistribuicao != null) {
            LocalDate dateNow = LocalDate.now();
            if (dateNow.isBefore(dataDistribuicao)) {
                throw new ValidationException("Data de distribuição deve ser menor ou igual a data atual.");
            }
        }
        
        if (id != null && id.equals(idProcessoPai)){
            throw new ValidationException("Id do processo Pai não pode ser o mesmo Id do processo.");
        }
    }

    @PreRemove()
    public void validadeDelete(){
        if (situacao.getFinalizado()){
            throw new ValidationException("Processo com situação finalizada não pode ser removido.");
        }
        
        if (processosFilhos != null && processosFilhos.size() > 1){
            throw new ValidationException("Processo com processos filhos não pode ser removido.");
        }
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroProcessoUnificado() {
        return numeroProcessoUnificado;
    }

    public void setNumeroProcessoUnificado(String numeroProcessoUnificado) {
        this.numeroProcessoUnificado = numeroProcessoUnificado;
    }

    public LocalDate getDataDistribuicao() {
        return dataDistribuicao;
    }

    public void setDataDistribuicao(LocalDate dataDistribuicao) {
        this.dataDistribuicao = dataDistribuicao;
    }

    public Boolean getSegredoJustica() {
        return segredoJustica;
    }

    public void setSegredoJustica(Boolean segredoJustica) {
        this.segredoJustica = segredoJustica;
    }

    public String getPastaFisicaCliente() {
        return pastaFisicaCliente;
    }

    public void setPastaFisicaCliente(String pastaFisicaCliente) {
        this.pastaFisicaCliente = pastaFisicaCliente;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Set<Responsavel> getResponsaveis() {
        return responsaveis;
    }

    public void setResponsaveis(Set<Responsavel> responsaveis) {
        this.responsaveis = responsaveis;
    }

    public Long  getIdProcessoPai() {
        return idProcessoPai;
    }

    public void setIdProcessoPai(Long idProcessoPai) {
        this.idProcessoPai = idProcessoPai;
    }

    public Set<Processo> getProcessosFilhos() {
        return processosFilhos;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.numeroProcessoUnificado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Processo other = (Processo) obj;
        if (!Objects.equals(this.numeroProcessoUnificado, other.numeroProcessoUnificado)) {
            return false;
        }
        return true;
    }

}
