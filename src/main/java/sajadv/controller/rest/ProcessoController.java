package sajadv.controller.rest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sajadv.domain.processo.Processo;
import sajadv.domain.processo.ProcessoFilter;
import sajadv.domain.processo.ProcessoRepository;
import sajadv.domain.processo.ProcessoService;

/**
 *
 * @author isaac.souza
 */
@RestController()
@CrossOrigin
public class ProcessoController {
    
    @Autowired
    private ProcessoRepository repository;
    
    @Autowired
    private ProcessoService service;
    
    @RequestMapping(value="/processos", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public Page<Processo> find(@RequestParam Map<String, String> queryParameters,
            Pageable pageable){
        
        ProcessoFilter filter = new ProcessoFilter(
                (!StringUtils.isEmpty(queryParameters.get("npu")) ? queryParameters.get("npu") : null), 
                (!StringUtils.isEmpty(queryParameters.get("dataDistribuicaoInicial")) ? 
                        LocalDate.parse(queryParameters.get("dataDistribuicaoInicial"),DateTimeFormatter.ISO_LOCAL_DATE_TIME) : null),
                (!StringUtils.isEmpty(queryParameters.get("dataDistribuicaoFinal")) ? 
                        LocalDate.parse(queryParameters.get("dataDistribuicaoFinal"),DateTimeFormatter.ISO_LOCAL_DATE_TIME) : null),
                (!StringUtils.isEmpty(queryParameters.get("segredoJustica")) ? Boolean.parseBoolean(queryParameters.get("segredoJustica")) : null),
                (!StringUtils.isEmpty(queryParameters.get("pastaFisicaCliente")) ? queryParameters.get("pastaFisicaCliente") : null),
                (!StringUtils.isEmpty(queryParameters.get("idSituacao")) ? Long.valueOf(queryParameters.get("idSituacao")): null),
                (!StringUtils.isEmpty(queryParameters.get("nomeResponsavel")) ? queryParameters.get("nomeResponsavel") : null));        
        Page<Processo> processos = service.find(filter, pageable);
        
        return processos;
    }
    
    @RequestMapping(value="/processos/resumido", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public List<Processo> findAllResumido(){
        List<Processo> processos = repository.findAllProcessosResumido();
        return processos;
    }
    
    @RequestMapping(value = "/processos/{id}", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public ResponseEntity findOne(@PathVariable Long id){
        
        Processo processo = repository.findOne(id);
        
        if (processo == null) {
            return ResponseEntity.notFound().build();
        }
        
        return ResponseEntity.ok(processo);
    }
    
    @RequestMapping(value = "/processos", method = RequestMethod.POST)
    @Transactional()
    public Processo create(@RequestBody Processo processo){
        
        repository.save(processo);
        
        return processo;
    }
    
    @RequestMapping(value = "/processos/{id}", method = RequestMethod.PUT)
    @Transactional()
    public ResponseEntity update(@PathVariable Long id, @RequestBody Processo processo){
        Processo processoSalvo = repository.findOne(id);
        
        if (processo == null) {
            return ResponseEntity.notFound().build();
        }else{
            processoSalvo.setDataDistribuicao(processo.getDataDistribuicao());
            processoSalvo.setDescricao(processo.getDescricao());
            processoSalvo.setNumeroProcessoUnificado(processo.getNumeroProcessoUnificado());
            processoSalvo.setPastaFisicaCliente(processo.getPastaFisicaCliente());
            processoSalvo.setIdProcessoPai(processo.getIdProcessoPai());
            processoSalvo.setResponsaveis(processo.getResponsaveis());
            processoSalvo.setSegredoJustica(processo.getSegredoJustica());
            processoSalvo.setSituacao(processo.getSituacao());
        }
        
        repository.save(processoSalvo);
        
        return ResponseEntity.ok(processoSalvo);
    }
    
    @RequestMapping(value = "/processos/{id}", method = RequestMethod.DELETE)
    @Transactional()
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity delete(@PathVariable Long id){
        Processo processo = repository.findOne(id);
        
        if (processo == null) {
            ResponseEntity.notFound().build();
        }
        
        repository.delete(processo);
        
        return ResponseEntity.ok().build();
    }
}
