package sajadv.controller.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sajadv.domain.processo.Situacao;
import sajadv.domain.processo.SituacaoService;

/**
 *
 * @author isaac.souza
 */
@RestController()
@CrossOrigin
public class SituacaoController {
    
    @Autowired
    private SituacaoService service;
    
    @RequestMapping(value="/situacoes", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public List<Situacao> findAll(){
        return  service.findAll();
    }
    
}
