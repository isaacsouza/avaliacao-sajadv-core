package sajadv.controller.rest;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sajadv.domain.responsavel.Responsavel;
import sajadv.domain.responsavel.ResponsavelFilter;
import sajadv.domain.responsavel.ResponsavelRepository;
import sajadv.domain.responsavel.ResponsavelService;

/**
 *
 * @author isaac.souza
 */
@RestController()
@CrossOrigin
public class ResponsavelController {
    
    @Autowired
    private ResponsavelRepository repository;
    
    @Autowired
    private ResponsavelService service;
    
    @RequestMapping(value="/responsaveis", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public Page<Responsavel> find(@RequestParam Map<String, String> queryParameters,
            Pageable pageable){
        
        ResponsavelFilter filter = new ResponsavelFilter(queryParameters.get("nome"), queryParameters.get("cpf"), queryParameters.get("npu"));        
        Page<Responsavel> responsaveis = service.find(filter, pageable);
        
        return responsaveis;
    }
    
    @RequestMapping(value="/responsaveis/resumido", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public List<Responsavel> findAllResumido(){
        List<Responsavel> responsaveis = repository.findAllResponsaveisResumido();        
        return responsaveis;
    }
    
    @RequestMapping(value = "/responsaveis/{id}", method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public ResponseEntity findOne(@PathVariable Long id){
        
        Responsavel responsavel = repository.findOne(id);
        
        if (responsavel == null) {
            return ResponseEntity.notFound().build();
        }
        
        return ResponseEntity.ok(responsavel);
    }
    
    @RequestMapping(value = "/responsaveis", method = RequestMethod.POST)
    @Transactional()
    public Responsavel create(@RequestBody Responsavel responsavel){
        
        repository.save(responsavel);
        
        return responsavel;
    }
    
    @RequestMapping(value = "/responsaveis/{id}", method = RequestMethod.PUT)
    @Transactional()
    public ResponseEntity update(@PathVariable Long id, @RequestBody Responsavel responsavel){
        Responsavel responsavelSalvo = repository.findOne(id);
        
        if (responsavel == null) {
            return ResponseEntity.notFound().build();
        }else{
            responsavelSalvo.setNome(responsavel.getNome());
            responsavelSalvo.setEmail(responsavel.getEmail());
            responsavelSalvo.setCpf(responsavel.getCpf());
            responsavelSalvo.setFoto(responsavel.getFoto());
        }
        
        repository.save(responsavelSalvo);
        
        return ResponseEntity.ok(responsavelSalvo);
    }
    
    @RequestMapping(value = "/responsaveis/{id}", method = RequestMethod.DELETE)
    @Transactional()
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity delete(@PathVariable Long id){
        Responsavel responsavel = repository.findOne(id);
        
        if (responsavel == null) {
            ResponseEntity.notFound().build();
        }
        
        repository.delete(responsavel);
        
        return ResponseEntity.ok().build();
    }
}
