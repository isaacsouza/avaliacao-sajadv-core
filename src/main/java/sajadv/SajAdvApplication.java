package sajadv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

/**
 * CLasse principal do sistema. Ela inicia o container.
 * 
 * @author isaacsouza
 */
@EntityScan(
        basePackageClasses = {SajAdvApplication.class, Jsr310JpaConverters.class}
)
@SpringBootApplication
public class SajAdvApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SajAdvApplication.class, args);
    }
    
}
