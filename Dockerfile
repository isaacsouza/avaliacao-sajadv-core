FROM openjdk:8-jdk-alpine

# Copy files to app directory
COPY . /app

WORKDIR /app

RUN chmod 775 -R /app/ && \
    ./gradlew bootRepackage

# Run the app
CMD sh run.sh